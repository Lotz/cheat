 _____________________________________________
/ This repository has been moved to codeberg. \
\ https://codeberg.org/ManfredLotz/cheat      /
 ---------------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

# cheat

Using cheat (https://github.com/cheat/cheat) this project enables me to add my own cheat sheets and to enhance existing cheat sheets

## Local config

Local config is in `/home/manfred/.config/cheat/conf.yml` and points to

- the community-sourced cheatsheets at `/usr/share/cheat`
- and my own additions in `~/cheat/personal`

Its content looks like this:

```
cheatpaths:
  - name: community                   # a name for the cheatpath
    path: /usr/share/cheat # the path's location on the filesystem
    tags: [ community ]               # these tags will be applied to all sheets on the path
    readonly: true                    # if true, `cheat` will not create new cheatsheets here

  - name: personal
    path: ~/cheat          # this is a separate directory and repository than above
    tags: [ personal ]
    readonly: false                   # new sheets may be 
```
